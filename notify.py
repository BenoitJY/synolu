#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''

---------------------------------------------------------------
                  Script SyNoLu
         Système de notification lumineuse
---------------------------------------------------------------
              Benoît Jahény @ 2020
'''

import notify2
import sys

def sendmessage(title, message):
    notify2.init("Test")
    notice = notify2.Notification(title, message)
    notice.show()
    return
titre = sys.argv[1]
contenu = sys.argv[2]
sendmessage(titre,contenu)
