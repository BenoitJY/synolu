#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''

---------------------------------------------------------------
                  Script SyNoLu
         Système de notification lumineuse
---------------------------------------------------------------
              Benoît Jahény @ 2020

Fichier des variables communes

'''
# Répertoire du script à éditer
path = "/home/benoit/Programmation/Synolu"

#--------------fin édition------------#

imgpath = path+"/img"
SyNoLu = "python3 "+path+"/synolu.py"
NoTiFy = "python3 "+path+"/notify.py"
