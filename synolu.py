#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''

---------------------------------------------------------------
                  Script SyNoLu
         Système de notification lumineuse
---------------------------------------------------------------
              Benoît Jahény @ 2020

synchro avec Birdtray en lui faisant envoyer "mail" à chaque nouveau mail ou remise à 0 du compteur
'''
from mesVars import *

import serial
import serial.tools.list_ports
import sys
import os
import notify2

def checkFileExistence(filePath):
    try:
        with open(filePath, 'r') as f:
            return True
    except FileNotFoundError as e:
        return False
    except IOError as e:
        return False

if len( sys.argv ) == 1:
    print( "Usage: synolu.py <NUM_COULEUR> (-h | --help)" )
    print( "NUM_COULEUR : un nombre à 2 chiffres pour commander la couleur.\n Le premier chiffre commande le clignotement ou non (0|1) et le second commande la couleur:" )
    print( "0 éteint la led; 1 rouge; 2 vert; 3 bleu; 4 violet; 5 orange; 6 jaune; 7 blanc 50%; 8 blanc; 9 cyan" )
    exit()

message = sys.argv[1]

if message == "mail" :

    if checkFileExistence(path+"/mail") :

        if int(sys.argv[2])<int(sys.argv[3]) :
            message = "13"
            with open(path+"/mail", "a") as f:
                f.write(sys.argv[2]+":"+sys.argv[3])
        else :
            message = "00"
            os.remove(path+"/mail")
    else :
        if int(sys.argv[2])<int(sys.argv[3]) :
            with open(path+"/mail", "w") as f:
                f.write(sys.argv[2]+":"+sys.argv[3])
            message = "13"

try:
    param = int( message )
except ValueError:
    print( "Mauvaise valeur transmise: %s" % sys.argv[1], file=sys.stderr )
    exit()

if (len(message)!=2):
    exit("le code couleur comporte obligatoirement deux chiffres")

if __name__ == '__main__':
    ports = serial.tools.list_ports.comports(include_links=False)
    if (len(ports) != 0): # on a trouvé au moins un port actif
        banco = 0
        for port in ports :  # affichage du nom de chaque port
            if "Digispark" in port.description:
                banco = 1
                arduino = serial.Serial(port.device)
        if banco == 0:
            print( "le périphérique Digispark n'a pas été trouvé." )
            exit()
        arduino.write(str(message).encode())   # envoi du message série, deux fois. Pourquoi deux? Parce que ça marche.
        arduino.write(str(message).encode())
    else: # on n'a pas trouvé de port actif
        print("Aucun port actif n'a ete trouve")
