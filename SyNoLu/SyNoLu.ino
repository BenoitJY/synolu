//
// Permettre de commander une led RVB en envoyant un nombre via la console série à l'attiny85
//
//
// Pense-bête définition des couleurs en fixe [0X] implémentées
//  [00][10][20] éteint    {0,0,0};
//  [01] rouge     {255,0,0};
//  [02] vert      {0,255,0};
//  [03] bleu      {0,0,255};
//  [04] violet    {255,0,255};
//  [05] orange    {255,20,0};
//  [06] jaune     {255,50,0};
//  [07] blanc50%  {127,50,25};
//  [08] blanc100% {255,100,50};
//  [09] cyan {0,255,255};
//       lime {255,100,0};

#include <DigiCDC.h>

const int redLEDPin = 0;
const int greenLEDPin = 1;
const int blueLEDPin = 2;

int marq=0;
char clign='0';
char coul;

const uint8_t PROGMEM gamma8[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };

// ------- Fonctions --------//

// allumer la LED aux valeurs RVB
void setRVB(int r, int v, int b) {
  analogWrite(redLEDPin, pgm_read_byte(&gamma8[r]));
  analogWrite(greenLEDPin, pgm_read_byte(&gamma8[v]));
  analogWrite(blueLEDPin, pgm_read_byte(&gamma8[b]));
}

//effet de pulsation, lumière blanche, vitesse et nombre de "tours" en paramètre
void effetRVBpulsationblanche(int vitesse,int nbr) {
   int i;
   int y;
   int r=0;
   int v=0;
   int b=0;
  for (y = 0; y < nbr; ++y){
    for (i = 0; i < 50; ++i) {
      analogWrite(redLEDPin, r);
      analogWrite(greenLEDPin, v);
      analogWrite(blueLEDPin, b);
      delay(vitesse);
      r=r+5;
      v=v+2;
      b=b+1;
      }
    for (i = 0; i < 50; ++i) {
      analogWrite(redLEDPin, r);
      analogWrite(greenLEDPin, v);
      analogWrite(blueLEDPin, b);
      delay(vitesse);
      r=r-5;
      v=v-2;
      b=b-1;
     }
   }
   setRVB(0,0,0);
}

//------------------------------------Main program-----------------------------------//
void setup() {
  // Lancer la connexion série USB, pas besoin de spécifier un bitrate
  SerialUSB.begin();
}

// the loop routine runs over and over again forever:
void loop() {
  if (SerialUSB.available()) {
    char input = SerialUSB.read(); // On lit ce qui est envoyé sur le serial

    // ----------- Lecture premier chiffre, 0 fixe, 1 clignote, (2 fade in/out à coder) ---------------//
    
    if (marq==0) { //début de lecture
      if (input=='0') {
        SerialUSB.write('a'); //déboguage
        clign='0';
        marq++;
      }
      else if (input=='1') {
        SerialUSB.write('b'); //déboguage
        clign = '1';
        marq++;
      }
    }

    // ----------------------- Lecture second chiffre, type de couleur ---------------------//

    else if (marq==1) {
      SerialUSB.write('c'); //déboguage
      // on envoie la couleur demandée
      if (input=='0')       setRVB(0,0,0);
      else if (input=='1')  setRVB(255,0,0);
      else if (input=='2')  setRVB(0,255,0);
      else if (input=='3')  setRVB(0,0,255);
      else if (input=='4')  setRVB(255,0,255);
      else if (input=='5')  setRVB(255,20,0);
      else if (input=='6')  setRVB(255,50,0);
      else if (input=='7')  setRVB(127,50,25);
      else if (input=='8')  setRVB(250,80,140);
      else if (input=='9')  setRVB(0,255,255);
      marq++;
      coul = input;
    }
    else if (marq==2) {
      SerialUSB.write('d'); //déboguage
      marq++;
    }
    else {
      SerialUSB.write(clign);
      marq=0;
    }
  }
  if (clign == '1') {
    if (coul == '1')  setRVB(255,0,0);
    else if (coul=='2')  setRVB(0,255,0);
    else if (coul=='3')  setRVB(0,0,255);
    else if (coul=='4')  setRVB(255,0,255);
    else if (coul=='5')  setRVB(255,20,0);
    else if (coul=='6')  setRVB(255,50,0);
    else if (coul=='7')  setRVB(127,50,25);
    else if (coul=='8')  setRVB(250,80,140);
    else if (coul=='9')  setRVB(0,255,255);
    delay(100);
    setRVB(0,0,0);
  }
 SerialUSB.delay(100);               // keep usb alive
}
