#!/usr/bin/env python3
# minimal menu in the systray
# arpinux, cyrille & David5647 @2020
# from https://debian-facile.org/viewtopic.php?id=28826
#
#nécessite gir1.2-appindicator3-0.1
#
#------------------------------------------------------
from mesVars import *

import os
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Gtk as gtk, AppIndicator3 as appindicator

LAUNCHERS = [
    {
        "label": "Ne pas déranger",
        "icon": imgpath+"/DoNotDisturb.png",
        "command": SyNoLu+" 01 &&"+NoTiFy+" 'Mode' 'Ne pas déranger activé'",
    },
    {
        "label": "Occupé",
        "icon": imgpath+"/occupied.png",
        "command": SyNoLu+" 06 &&"+NoTiFy+" 'Mode' 'Occupé activé'",
    },
    {
        "label": "Libre",
        "icon": imgpath+"/free.png",
        "command": SyNoLu+" 02 &&"+NoTiFy+" 'Mode' 'Libre activé'",
    },
    {
        "label": "Éteindre",
        "icon": imgpath+"/black.png",
        "command": SyNoLu+" 00 &&"+NoTiFy+" 'SyNoLU' 'Extinction'",
    }
]

def main():
    indicator = appindicator.Indicator.new(
        "customtray",
        path+"/img/logo.png",
        appindicator.IndicatorCategory.APPLICATION_STATUS,
    )
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(menu())
    gtk.main()

def create_menu_item(label=None, icon=None, command=None, sep=False):

    if not sep:
        img = gtk.Image()
        img.set_from_file(icon)
        cmd = gtk.ImageMenuItem(label=label)
        cmd.set_image(img)
        cmd.connect("activate", lambda _ : os.system(command))
        return cmd
    else:
        return gtk.SeparatorMenuItem()

def menu():
    menu = gtk.Menu()
    for launcher in LAUNCHERS:
        menu.append(create_menu_item(**launcher))

    exittray = gtk.ImageMenuItem(label='close tray')
    img = gtk.Image()
    img.set_from_file(path+"/img/close.png")
    exittray.set_image(img)
    exittray.connect('activate', quit)
    menu.append(exittray)

    menu.show_all()
    return menu

if __name__ == "__main__":
    main()
