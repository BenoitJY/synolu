# SyNoLu Documentation

## What is SyNoLu?

**Sy**stème de **No**tification **Lu**mineuse (light notification system in french)

Build with a digispark and RGB led, It's a visual indicator to tell each other if you are:
* free (to do whatever you want...)
* busy
* do not disturb state

it also allows, by interfacing with [BirdTray](https://github.com/gyunaev/birdtray) [^1] to notify the arrival of new messages (experimental, work in progress)

### Files in SyNoLu
* synolu.py : file to transmit information to attiny85
* main.py : main menu, launch in the systray
* notify.py : call by synolu.py, to notify which mode is selected by main.py

## Licence
free for the parts I wrote (synolu.* - notify.py - pictures)
For main.py, i don't know.

## Requirement
![SyNoLu Schema](/img/breadboard.png)
### for building
* a digispark, or something like that
* rgb led
* 3 x 220 &Omega; resistor
* arduino with DigiCDC (see: [https://digistump.com/wiki/digispark/tutorials/connecting](https://digistump.com/wiki/digispark/tutorials/connecting) )

### for using
* python3 with os, serial, sys
* python3-notify2 and pyserial (typically : pip install pyserial notify2)
* gir1.2-appindicator3-0.1 (for debian/ubuntu. Other, i don't know)
* this scripts (main.py, mesVars.py, synolu.py, and img folder)

## Using
1. put all files in a folder
2. in mesVars.py, change the 'path' variable
3. execute by "python3 /path/to/the/script/main.py &"

You can also use synolu.py directly, open the file and read how you can do it, it's easy to understand.

I pecked at various sources to code this little "thing", it is intended for my personal use, I deliver it without guarantee of any kind, including that of being the author ... I tried to source them elements that I have recovered, if you recognize one of your codes and you WANT me to report it, let me know

Have fun

[^1]:in the monitoring section, put : `python /path/to/synolu.py "mail" %OLD% %NEW%"`. You don't need to run main.py for that.
